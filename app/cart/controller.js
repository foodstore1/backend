const { policyFor } = require('../policy');
const Product = require('../product/model');
const CartItem = require('../cart-item/model');

async function index(req, res, next){
    let policy = policyFor(req.user);
    if(!policy.can('read', 'Cart')){
        return res.json({
            error: 1, 
            message: `You're not allowed to perform this action`
        });
    }

    try{
        // (1) cari items dari MongoDB berdasarkan `user`
        let items = await CartItem
        .find({user: req.user._id})
        .populate('product');

        // (2) respone ke _client_
        return res.json(items);
    } catch(err) {
        if(err && err.name == 'ValidationError'){
            return res.json({
                error: 1, 
                message: err.message, 
                fields: err.errors
            });
        }
        next(err)
    }
}

async function update(req, res, next) {
    let policy = policyFor(req.user);
    if (!policy.can('update', 'Cart')) {
        return res.json({
            error: 1,
            message: `You're not allowed to perform this action`
        });
    }

    try {
        // (1) dapatkan `payload` `items`
        const { items } = req.body;
        // (1) ekstrak `_id` dari masing-masing `item`
        const productIds = items.map(itm => itm.product._id);
        // (1) cari data produk di MongoDB simpan sbg `products`
        const products = await Product.find({_id: {$in: productIds}})

        // (1) persiapkan data `cartItem`
        let cartItems = items.map(item => {
            let relatedProduct = products.find(product =>
            product._id.toString() === item.product._id);
            // (1) buat objek yang memuat informasi untuk disimpan sbg `CartItem`
            return { 
                product: relatedProduct._id, 
                price: relatedProduct.price, 
                image_url: relatedProduct.image_url, 
                name: relatedProduct.name, 
                user: req.user._id, 
                qty: item.qty
            }
        });

        await CartItem.deleteMany({user: req.user._id});
        // update / simpan ke MongoDB semua item pada `cartItems`
        await CartItem.bulkWrite(cartItems.map(item => {
            return {
                updateOne: {
                    filter: {user: req.user._id, product: item.product}, 
                    update: item, 
                    upsert: true
                    // upsert: true kita memerintahkan MongoDB untuk membuat record baru jika tidak ada dokumen ditemukan.
                } 
            }
        }));

        return res.json(cartItems);
    } catch (err) {
        if(err && err.name == 'ValidationError'){
            return res.json({
                error: 1, 
                message: err.message, 
                fields: err.errors
            });
        }
        next(err)
    }
}

module.exports = {
    index,
    update
}