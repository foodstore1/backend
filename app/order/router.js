// (1) import `router` dan `multer`
const router = require('express').Router(); 
const multer = require('multer');
// (2) import `orderController`
const orderController = require('./controller');

router.get('/orders', orderController.index);
// (3) _route_ untuk membuat `order`
router.post('/orders', multer().none(), orderController.store);

// (4) export `router`
module.exports = router;