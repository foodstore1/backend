const mongoose = require('mongoose');
const Order = require('./model');
const OrderItem = require('../order-item/model');
const CartItem = require('../cart-item/model');
const DeliveryAddress = require('../delivery-address/model');
const { policyFor } = require('../policy');
const { subject } = require('@casl/ability');

async function index(req, res, next){
    let policy = policyFor(req.user);
    if(!policy.can('view', 'Order')){
        return res.json({
            error: 1, 
            message: `You're not allowed to perform this action`
        });
    }

    try{
        // (1) dapatkan `limit` dan `skip` dari _query string_
        let { limit = 10, skip = 0 } = req.query;

        let count = await Order
        .find({user: req.user._id})
        .countDocuments();

        let orders = await Order 
        .find({user: req.user._id})
        .limit(parseInt(limit))
        .skip(parseInt(skip))
        .populate('order_items')
        .sort('-createdAt'); // descending (menggunakan minus sebagai awalan)

        return res.json({
            data: orders.map(order => order.toJSON({virtuals: true})),
            // karena schema Order memiliki field virtual yaitu items_count, ingat kan? Jika kita tidak
            // menggunakan kode di atas toJSON({virtuals: true}), maka field tersebut tidak akant ersedia
            // pada saat diubah menjadi JSON.
            count
        });
    } catch(err){
        if(err && err.name == 'ValidationError'){
            return res.json({
                error: 1, 
                message: err.message, 
                fields: err.errors,
            });
        }
        next(err);
    }
}

async function store(req, res, next){
    // (1) dapatkan policy untuk user yang sedang login
    let policy = policyFor(req.user);
    // (2) cek apakah policy mengizinkan untuk membuat order
    if(!policy.can('create', 'Order')){
        return res.json({
            error: 1, 
            message: `You're not allowed to perform this action`
        });
    }

    try{
        // (1) dapatkan `delivery_fee` dan `delivery_address`
        let { delivery_fee, delivery_address } = req.body;
        let items = await CartItem
        .find({user: req.user._id})
        .populate('product');

        // cek apakah keranjang belanja kosong?
        if(!items.length){
            return res.json({
                error: 1, 
                message: `Can not create order because you have no items in cart`
            });
        }

        let address = await DeliveryAddress.findOne({_id: delivery_address});

        // create order but don't save it yet. 
        // using mongoose.Types.ObjectId() to generate id for saving ref
        let order = new Order({
            _id: new mongoose.Types.ObjectId(), 
            // kita memberikan _id secara eksplisit dengan nilai new
            // mongoose.Types.ObjectId(). Kenapa demikian? Karena sebelum menyimpan ke MongoDB, kita
            // ingin merelasikan objek order ini ke OrderItems yang akan kita buat.
            status: 'waiting_payment', 
            delivery_fee, 
            delivery_address: {
                provinsi: address.provinsi, 
                kabupaten: address.kabupaten, 
                kecamatan: address.kecamatan, 
                kelurahan: address.kelurahan, 
                detail: address.detail
            },
            user: req.user._id
        });

        // create order items too
        let orderItems = await OrderItem
        .insertMany(
            items.map(item => ({
                ...item, 
                name: item.product.name,
                qty: parseInt(item.qty), 
                price: parseInt(item.product.price),
                order: order._id,
                product: item.product._id
            })
        ));

        orderItems.forEach(item => order.order_items.push(item));

        order.save();

        // clear cart items
        await CartItem.deleteMany({user: req.user._id});

        return res.json(order);

    } catch(err){
        if(err && err.name == 'ValidationError'){
            return res.json({
                error: 1, 
                message: err.message, 
                fields: err.errors
            });
        }
        next(err);
    }
}

module.exports = {
    index,
    store
}